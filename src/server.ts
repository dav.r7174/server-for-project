// src/server.ts
import express from "express";
import cors from "cors";

const app = express();

app.use(cors());
app.use(express.json());

app.get("/", (rec, res) => {
  res.send("connected");
});
// Trip routes

app.listen(3020, () => {
  console.log(`Server is up and running`);
});
