#!/bin/sh

if [ "$#" -ne 1 ]
then
  PORT=3020
else
  PORT=$1
fi

export PORT=$PORT
npm run dev